from django.shortcuts import render
from todos.models import TodoList, TodoItem
from django.urls import reverse, reverse_lazy

from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView

# Create your views here.

class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"

class TodoListDetail(DetailView):
    model = TodoList
    template_name = 'todos/detail.html'

class TodoListCreate(CreateView):
    model = TodoList
    template_name = 'todos/create.html'
    fields = ['name']
    
    def get_success_url(self): # look into get success url
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.id})
    

class TodoListUpdate(UpdateView):
    model = TodoList
    template_name = 'todos/edit.html'
    fields = ['name']

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.id})

class TodoListDelete(DeleteView):
    model = TodoList
    template_name = 'todos/delete.html'
    success_url = reverse_lazy('todo_list_list')

class TodoItemCreate(CreateView):
    model = TodoItem
    template_name = 'todoitem/create.html'
    fields = ['task', 'due_date', 'is_completed', 'list']

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.list.id})

class TodoItemUpdate(UpdateView):
    model = TodoItem
    template_name = 'todoitem/edit.html'
    fields = ['task', 'due_date', 'is_completed', 'list'] 

    def get_success_url(self):
        return reverse_lazy('todo_list_detail', kwargs={'pk': self.object.list.id})